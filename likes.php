<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amCharts examples</title>
        <link rel="stylesheet" href="style.css" type="text/css">
        <script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>         
        <script type="text/javascript">
            
            // this chart is exactly the same as in areaStacked.html, only made using JSON except JavaScript API

            var chartData = [{
               "month": 08,
                   "likes": 1587
                   
                   
           }, {
               "month": 09,
                   "likes": 1567
                   
           }, {
               "month": 10,
                   "likes": 1617
                   
           }, {
               "month": 10,
                   "likes": 1630
                  
           }, {
               "month": 10,
                   "likes": 1660
                   
           }, {
               "month": 10,
                   "likes": 1683
                   
           }, {
               "month": 11,
                   "likes": 1691
                   
           }, {
               "month": 11,
                   "likes": 1298
                   
           },  {
               "month": 12,
                   "likes": 1159
                   
           }];

           AmCharts.makeChart("chartdiv", {
               type: "serial",
               pathToImages: "images/",
               dataProvider: chartData,
               marginTop: 10,
               categoryField: "month",
               categoryAxis: {
                   gridAlpha: 0.07,
                   axisColor: "#DADADA",
                   startOnAxis: true,
                   guides: [{
                       category: "08",
                       lineColor: "#CC0000",
                       lineAlpha: 1,
                       dashLength: 2,
                       inside: true,
                       labelRotation: 90,
                       label: "fines for speeding increased"
                   }, {
                       category: "09",
                       lineColor: "#CC0000",
                       lineAlpha: 1,
                       dashLength: 2,
                       inside: true,
                       labelRotation: 90,
                       label: "motorcycle maintenance fee introduced"
                   }]
               },
               valueAxes: [{
                   stackType: "regular",
                   gridAlpha: 0.07,
                   title: "Total Likes"
               }],

               graphs: [{
                   type: "line",
                   title: "Likes",
                   hidden: true,
                   valueField: "likes",
                   lineAlpha: 0,
                   fillAlphas: 0.6,
                   balloonText: "<img src='images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>"
               }],
               legend: {
                   position: "top",
                   valueText: "[[value]]",
                   valueWidth: 100,
                   valueAlign: "left",
                   equalWidths: false,
                   periodValueText: "total: [[value.sum]]"
               },
               chartCursor: {
                   cursorAlpha: 0
               },
               chartScrollbar: {
                   color: "FFFFFF"
               }

           });

        </script>
    </head>
    
    <body>
        <div id="chartdiv" style="width:100%; height:400px;"></div>
    </body>

</html>